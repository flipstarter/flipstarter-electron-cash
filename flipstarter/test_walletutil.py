from io import StringIO
from unittest import mock
import os
import shutil
import sys
import tempfile
import unittest

from electroncash.keystore import from_xprv
from electroncash.keystore import Hardware_KeyStore
from electroncash.simple_config import SimpleConfig
from electroncash.storage import WalletStorage
from electroncash.wallet import Address
from electroncash.wallet import create_new_wallet
from electroncash.wallet import ImportedPrivkeyWallet
from electroncash.wallet import Multisig_Wallet
from electroncash.wallet import restore_wallet_from_text

from .walletutil import _coin_to_self_tx
from .walletutil import _strict_broadcast
from .walletutil import ERR_GEN_ADDR
from .walletutil import ERR_HARDWARE
from .walletutil import ERR_MULTISIG
from .walletutil import ERR_SLP
from .walletutil import ERR_WATCHINGONLY
from .walletutil import is_wallet_compatible
from .walletutil import spend_coin_to_self
from .walletutil import get_wallet_address


MNEMONIC = 'head frost nest keep flavor winner pretty mimic truly sense snack laugh'
XPUB = "xpub6CUzEfgtza7ZNtfDGYwHPnbPMPiQh93mAbP6v7C3ozUgkZq4tXSgYb9qqZ62oh8RCeexdSF7ZJmTzCm5bdWLB3zSMF8rNfuY8kccNAsdF4d"
XPRV = "xprv9y4nb6Akxru8R68sYGrihutfqUgMNxmiF83ViTf65MobJrRRyHWc1M8mSZJSmZ1nQCJntxmF99sKGkkcQQGziECvdkwA4kqxsH5srNAzRin"
PRIVKEY = "L4vmKsStbQaCvaKPnCzdRArZgdAxTqVx8vjMGLW5nHtWdRguiRi1"


class WalletBasicsTestCase(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.tmpdir = tempfile.mkdtemp()
        self.config = SimpleConfig({'electron_cash_path': self.tmpdir})

        # Mutes _most_ of electron cash logging
        self._saved_stdout = sys.stdout
        self._saved_stderr = sys.stderr
        self._out_buffer = StringIO()
        sys.stdout = self._out_buffer
        sys.stderr = self._out_buffer

    def tearDown(self):
        super().tearDown()
        shutil.rmtree(self.tmpdir)

        # Restore the "real" stdout and stderr
        sys.stderr = self._saved_stdout
        sys.stderr = self._saved_stderr

    def random_wallet_path(self):
        import uuid
        return os.path.join(self.tmpdir, uuid.uuid4().hex)

class TestWalletCompatibility(WalletBasicsTestCase):
    def test_new_wallet(self):
        wallet = create_new_wallet(
            path = self.random_wallet_path(),
            config = self.config)['wallet']
        ok, err = is_wallet_compatible(wallet)
        self.assertTrue(ok)
        self.assertEqual(err, None)

    def test_wallet_from_xpub(self):
        wallet = restore_wallet_from_text(XPUB,
            path = self.random_wallet_path(),
            config = self.config)['wallet']

        ok, err = is_wallet_compatible(wallet)
        self.assertFalse(ok)
        self.assertEqual(err, ERR_WATCHINGONLY)

    def test_wallet_xpriv(self):
        wallet = restore_wallet_from_text(XPRV,
            path = self.random_wallet_path(),
            config = self.config)['wallet']

        ok, err = is_wallet_compatible(wallet)
        self.assertTrue(ok)
        self.assertEqual(err, None)

    def test_wallet_from_mnemonic(self):
        wallet = restore_wallet_from_text(MNEMONIC,
            path = self.random_wallet_path(),
            config = self.config)['wallet']

        ok, err = is_wallet_compatible(wallet)
        self.assertTrue(ok)
        self.assertEqual(err, None)

    def test_slp_wallet(self):
        wallet = create_new_wallet(
            path = self.random_wallet_path(),
            config = self.config)['wallet']

        wallet.storage.put('wallet_type', 'Dummy SLP string')
        ok, err = is_wallet_compatible(wallet)
        self.assertFalse(ok)
        self.assertEqual(err, ERR_SLP)

    def test_imported_privkey(self):
        wallet = restore_wallet_from_text(PRIVKEY,
            path = self.random_wallet_path(),
            config = self.config)['wallet']
        self.assertTrue(isinstance(wallet, ImportedPrivkeyWallet))

        ok, err = is_wallet_compatible(wallet)
        self.assertFalse(ok)
        self.assertEqual(err, ERR_GEN_ADDR)

    def test_hardware_wallet(self):
        wallet = create_new_wallet(
            path = self.random_wallet_path(),
            config = self.config)['wallet']

        hwkeys = Hardware_KeyStore({
            'xpub' : XPUB,
            'label': "Dummy",
            'derivation': None
        })
        wallet.keystore = hwkeys
        ok, err = is_wallet_compatible(wallet)
        self.assertFalse(ok)
        self.assertEqual(err, ERR_HARDWARE)

    def test_multisig_wallet(self):
        store = WalletStorage(self.random_wallet_path())
        store.put('wallet_type', "1of1")
        store.put('x1/', from_xprv(XPRV).dump())
        wallet = Multisig_Wallet(store)

        ok, err = is_wallet_compatible(wallet)
        self.assertFalse(ok)
        self.assertEqual(err, ERR_MULTISIG)


class TestCancellations(WalletBasicsTestCase):
    def setUp(self):
        super().setUp()
        self.wallet = create_new_wallet(
            path=self.random_wallet_path(),
            config=self.config)['wallet']
        self.pledge_coin = _basic_coin("1111", 1)

    @mock.patch(f'{__package__}.walletutil._strict_broadcast')
    def test_spend_coin_to_self_broadcasts_signed_tx_and_saves_label(self, m_strict_broadcast):
        # setup the result of broadcast to confirm it is used in the label
        m_strict_broadcast.return_value = "expected txid"

        # spend the coin
        spend_coin_to_self(self.wallet, None, self.config, self.pledge_coin, "expected label")

        # confirm that the label has been stored using the result of broadcast
        self.assertEqual(self.wallet.get_label("expected txid"), "expected label")

    def test_coin_to_self_tx_uses_exactly_one_input_with_provided_coin(self):
        # patch new address generator to confirm it is used in the tx
        # make the tx
        tx = _coin_to_self_tx(self.wallet, self.config, self.pledge_coin)

        # confirm making a transaction with provided coin
        self.assertEqual(len(tx.inputs()), 1)
        actual_input_coin = tx.inputs()[0]
        self.assertEqual(actual_input_coin, self.pledge_coin)

    def test_coin_to_self_tx_uses_exactly_one_output_with_new_internal_address(self):
        # patch new address generator to confirm it is used in the tx
        expected_address = self.pledge_coin["address"]
        with mock.patch.object(self.wallet, "get_unused_address") as m_create_new_address:
            m_create_new_address.return_value = expected_address

            # make the tx
            tx = _coin_to_self_tx(self.wallet, self.config, self.pledge_coin)

        # confirm using exactly new address
        self.assertEqual(len(tx.outputs()), 1)
        actual_address = tx.outputs()[0][1]
        self.assertEqual(actual_address, expected_address)

    def test_coin_to_self_tx_uses_reasonable_fee(self):
        # make the tx
        tx = _coin_to_self_tx(self.wallet, self.config, self.pledge_coin)

        # confirm that a reasonable fee was used
        self.assertLess(tx.get_fee(), 1000)


class TestStrictBroadcast(unittest.TestCase):
    def test_strict_broadcast_broadcasts_in_normal_case(self):
        # setup a normal broadcast: network available, broadcast ok
        m_wallet = mock.MagicMock()
        ok = True
        expected_details = ""
        m_wallet.network.broadcast_transaction.return_value = ok, expected_details

        # run the test and confirm all ok
        _ = mock.MagicMock()
        actual_details = _strict_broadcast(m_wallet, _)
        self.assertEqual(actual_details, expected_details)

    def test_strict_broadcast_raises_ioerror_when_network_not_available(self):
        # setup a bad network
        m_wallet = mock.MagicMock()
        m_wallet.network = None
        _ = mock.MagicMock()
        self.assertRaises(IOError, _strict_broadcast, m_wallet, _)

    def test_strict_broadcast_raises_ioerror_when_broadcast_fails(self):
        # setup a failed broadcast
        m_wallet = mock.MagicMock()
        not_ok = False
        expected_details = ""
        m_wallet.network.broadcast_transaction.return_value = not_ok, expected_details

        _ = mock.MagicMock()
        self.assertRaises(IOError, _strict_broadcast, m_wallet, _)


def _basic_coin(txid, index,
                address='bitcoincash:qre0a8c9xfhr5xs9l2scwcq8rjlfxndq5ypfh7fg0q',
                value=0.1):
    address = address if isinstance(address, Address) else Address.from_string(str(address))
    return {
        "address": address,
        "prevout_hash": txid,
        "prevout_n": int(index),
        "type": "p2pkh",
        "value": value,
    }

class TestWalletAddreses(WalletBasicsTestCase):
    def setUp(self):
        super().setUp()
        self.wallet = create_new_wallet(
            path=self.random_wallet_path(),
            config=self.config)['wallet']

    def test_get_wallet_address(self):
        with mock.patch.object(self.wallet, "create_new_address") as c:
            c.return_value = "new"

            # There exists an unused, use that.
            with mock.patch.object(self.wallet, "get_unused_address") as u:
                u.return_value = "old"
                self.assertEqual("old", get_wallet_address(self.wallet))

            # There does not exists an unused, create new.
            with mock.patch.object(self.wallet, "get_unused_address") as u:
                u.return_value = None
                self.assertEqual("new", get_wallet_address(self.wallet))
