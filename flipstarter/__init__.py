import sys
import importlib.util
import os
from pathlib import Path

def fix_import():
    spec = importlib.util.find_spec('electroncash')
    if not spec:
        # Running for unit tests. We need to add Electron-Cash to the Python path.
        # Assumes Electron-Cash is checked out in ~/Electron-Cash
        ec_path = os.getenv("ELECTRON_CASH_HOME",
            os.path.join(Path.home(), "Electron-Cash"))

        sys.path.insert(0, ec_path)
        spec = importlib.util.find_spec('electroncash')
        if not spec:
            raise ImportError(
                f'Electron-Cash not found at {ec_path}\n'
                'Use env var ELECTRON_CASH_HOME to change path'
                )

fix_import()

from electroncash.i18n import _

fullname = "Flipstarter"
description = _("Raise funds cooperatively")
available_for = ['qt']
